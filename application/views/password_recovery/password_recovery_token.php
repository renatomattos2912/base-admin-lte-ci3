<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="login-box-body">
	<p class="login-box-msg">Recupere seu acesso informando a nova senha abaixo:</p>

	<form action="/" method="post">
		<div class="form-group has-feedback">
			<input type="email" class="form-control" placeholder="Senha">
			<span class="fa fa-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="email" class="form-control" placeholder="Repita a senha">
			<span class="fa fa-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button type="submit" class="btn btn-primary btn-block btn-flat">Redefinir senha</button>
			</div>
		</div>
	</form>
	<hr>
	<a href="/login">Voltar ao login</a><br>
	<a href="/signup" class="text-center">Fazer cadastro</a>
</div>
