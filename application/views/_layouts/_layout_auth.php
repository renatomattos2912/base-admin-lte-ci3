<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('_includes/_include_head.php'); ?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="/"><b>Admin</b>LTE</a>
	</div>

	<?php $this->load->view($main_content); ?>
</div>

<?php $this->load->view('_includes/_include_end-body.php'); ?>

</body>
</html>
