<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('_includes/_include_head.php'); ?>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
	<?php $this->load->view('_includes/_include_topbar.php'); ?>

	<div class="content-wrapper">
		<div class="container">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Top Navigation
					<small>Example 2.0</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Layout</a></li>
					<li class="active">Top Navigation</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<?php $this->load->view($main_content); ?>
			</section>
		</div>
	</div>

	<footer class="main-footer">
		<div class="container">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.3.8
			</div>
			<strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All
			rights
			reserved.
		</div>
	</footer>

	<?php $this->load->view('_includes/_include_end-body.php'); ?>

</body>
</html>
