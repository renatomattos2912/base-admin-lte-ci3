<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('admin/_includes/_include_head.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('admin/_includes/_include_topbar.php'); ?>
	<?php $this->load->view('admin/_includes/_include_sidebar.php'); ?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Dashboard
				<small>Version 2.0</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<?php $this->load->view($main_content); ?>
		</section>
	</div>

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 2.3.8
		</div>
		<strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
		reserved.
	</footer>

	<div class="control-sidebar-bg"></div>
</div>

<?php $this->load->view('admin/_includes/_include_end-body.php'); ?>

</body>
</html>
