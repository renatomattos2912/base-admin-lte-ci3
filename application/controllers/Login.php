<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['main_content'] = 'login/login';
		$this->load->view('_layouts/_layout_auth', $data);
	}
}

