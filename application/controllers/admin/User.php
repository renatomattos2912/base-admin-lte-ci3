<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['main_content'] = 'admin/user/user';
		$this->load->view('admin/_layouts/_layout_default', $data);
	}
}

