<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Password_recovery extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['main_content'] = 'password_recovery/password_recovery';
		$this->load->view('_layouts/_layout_auth', $data);
	}

	public function token($token) {
		$data['main_content'] = 'password_recovery/password_recovery_token';
		$this->load->view('_layouts/_layout_auth', $data);
	}
}

